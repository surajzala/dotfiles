call plug#begin('~/.local/share/nvim/plugged')

" start jedi conf
Plug 'davidhalter/jedi-vim'

" disable autocompletion since we have deoplete
let g:jedi#completions_enabled = 0

" open the go-to function in split, not another buffer
let g:jedi#use_splits_not_buffers = "right"

" end jedi conf

" deoplete config section "
Plug 'Shougo/deoplete.nvim', {'do': ':UpdateRemotePlugins'}
let g:deoplete#enable_at_startup = 1
autocmd InsertLeave,CompleteDone * if pumvisible() == 0 | pclose | endif
inoremap <expr><tab> pumvisible() ? "\<c-n>" : "\<tab>"
" end deoplete config section "

" start vim-airline conf
Plug 'vim-airline/vim-airline'

Plug 'vim-airline/vim-airline-themes'
let g:airline_theme='<theme>' " theme is apparently a valid theme name

" end vim-airline conf

Plug 'jiangmiao/auto-pairs'

Plug 'scrooloose/nerdcommenter'

" start nerdtree conf
Plug 'scrooloose/nerdtree'

nnoremap <silent> <C-k><C-B> :NERDTreeToggle<CR>

augroup nerdtree_open
	autocmd!
	autocmd VimEnter * NERDTree | wincmd p
augroup end


Plug 'terryma/vim-multiple-cursors'

" start neoformat conf
Plug 'sbdchd/neoformat'

" enable alignment
let g:neoformat_basic_format_align = 1

" enable tab to space conversion
let g:neoformat_basic_format_retab = 1

" enable trimming of trailing whitespace
let g:neoformat_basic_format_trim = 1

" end neoformat conf


call plug#end()


